import { createApp } from 'vue'
import './style.css'
import { createRouter, createWebHashHistory } from 'vue-router';

import App from './App.vue'
import Header from '@/components/Header.vue';
import ErrorText from '@/components/ErrorText.vue';
import Loading from '@/components/Loading.vue';
import Alert from '@/components/Alert.vue';
import { store } from './data/store';

const routes = createRouter({
    history: createWebHashHistory(),
    routes: [
        { path: '/', name: 'login', component: () => import('@/views/Login.vue') },
        { path: '/sign-up', name: 'signup', component: () => import('@/views/Register.vue') },
        { path: '/home', name: 'home', meta: { requiresAuth: true }, component: () => import('@/views/Home.vue') },
        { path: '/:pathMatch(.*)*', name: 'not-found', component: () => import('@/views/404.vue') },
    ]
});

routes.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        if (localStorage.getItem('_token') == null) {
            next({ name: 'login' });
        } else {
            next();
        }
    } else {
        if (localStorage.getItem('_token') == null)
            next();
        else
            next({ name: 'home' });
        }
    })

const app = createApp(App);

app.use(routes);

app.component('Header', Header);
app.component('error-text', ErrorText);
app.component('loading', Loading);
app.component('alert', Alert);

app.mount('#app');
