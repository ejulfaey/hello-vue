// store.js
import { reactive } from 'vue'

export const store = reactive({
  isLoading: false,
  alert: {
    show: false,
    success: true,
    message: ''
  },
})
